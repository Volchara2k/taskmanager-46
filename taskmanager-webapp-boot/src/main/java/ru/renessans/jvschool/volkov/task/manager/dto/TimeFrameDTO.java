package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "All details about the time frame")
public final class TimeFrameDTO implements Serializable {

    @Nullable
    @ApiModelProperty(
            value = "Date the task was created",
            name = "creationDate",
            example = "2021-03-01 13:52:41"
    )
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date creationDate;

    @Nullable
    @ApiModelProperty(
            value = "Date the task was started",
            example = "2021-03-01 13:52:41",
            name = "startDate"
    )
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date startDate;

    @Nullable
    @ApiModelProperty(
            value = "Date the task was ended",
            example = "2021-03-01 13:52:41",
            name = "endDate"
    )
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date endDate;

}