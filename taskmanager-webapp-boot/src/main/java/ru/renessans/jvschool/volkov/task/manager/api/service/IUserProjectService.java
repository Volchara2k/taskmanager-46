package ru.renessans.jvschool.volkov.task.manager.api.service;

import ru.renessans.jvschool.volkov.task.manager.entity.Project;

public interface IUserProjectService extends IUserOwnerService<Project> {
}