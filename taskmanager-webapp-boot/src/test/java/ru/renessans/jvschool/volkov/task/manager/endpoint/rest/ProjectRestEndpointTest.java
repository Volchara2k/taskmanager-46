package ru.renessans.jvschool.volkov.task.manager.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.renessans.jvschool.volkov.task.manager.AbstractWebApplicationTest;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Category({PositiveImplementation.class, EndpointImplementation.class})
public class ProjectRestEndpointTest extends AbstractWebApplicationTest {

    @NotNull
    private final String baseUrl = "/api";

    @Test
    public void createTest() throws Exception {
        @NotNull final String title = "title";
        @NotNull final String description = "description";
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setTitle(title);
        projectDTO.setDescription(description);

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        @NotNull final String requestJson = writer.writeValueAsString(projectDTO);

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/project/create")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value(title))
                .andExpect(jsonPath("$.description").value(description));
    }

    @Test
    public void updateByIdTest() throws Exception {
        @NotNull final String title = "title";
        @NotNull final String description = "description";
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(PROJECT.getId());
        projectDTO.setTitle(title);
        projectDTO.setDescription(description);

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        @NotNull final String requestJson = writer.writeValueAsString(projectDTO);

        assert PROJECT.getId() != null;
        this.mockMvc.perform(MockMvcRequestBuilders
                .put(baseUrl + "/project/edit")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(PROJECT.getId()))
                .andExpect(jsonPath("$.title").value(title))
                .andExpect(jsonPath("$.description").value(description));
    }

    @Test
    public void getByIdTest() throws Exception {
        assert PROJECT.getId() != null;
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/project/view/{id}", PROJECT.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(PROJECT.getId()));
    }

    @Test
    public void getAllTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/projects"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void deleteByIdTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(baseUrl + "/project/delete/{id}", PROJECT.getId()))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

}