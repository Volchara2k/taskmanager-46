package ru.renessans.jvschool.volkov.task.manager.listener.security;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AuthenticationEndpoint;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;

@RequiredArgsConstructor
public abstract class AbstractSecurityListener extends AbstractListener {

    @NotNull
    protected final AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

}