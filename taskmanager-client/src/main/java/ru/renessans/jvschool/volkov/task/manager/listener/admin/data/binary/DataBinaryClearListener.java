package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class DataBinaryClearListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_BIN_CLEAR = "data-bin-clear";

    @NotNull
    private static final String DESC_BIN_CLEAR = "очистить бинарные данные";

    @NotNull
    private static final String NOTIFY_BIN_CLEAR = "Происходит процесс очищения бинарных данных...";

    public DataBinaryClearListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_BIN_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_BIN_CLEAR;
    }

    @Async
    @Override
    @EventListener(condition = "@dataBinaryClearListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        final boolean clear = super.adminDataInterChangeEndpoint.dataBinClear(current);
        ViewUtil.print(NOTIFY_BIN_CLEAR);
        ViewUtil.print(clear);
    }

}