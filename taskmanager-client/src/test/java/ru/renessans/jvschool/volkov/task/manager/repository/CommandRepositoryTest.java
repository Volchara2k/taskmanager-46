package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.Collection;

@Setter
@RunWith(SpringRunner.class)
@Category(IntegrationImplementation.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public final class CommandRepositoryTest {

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.commandRepository);
    }

    @Test
    public void testGetAllCommands() {
        @Nullable final Collection<String> allCommands = this.commandRepository.getAllCommands();
        Assert.assertNotNull(allCommands);
    }

    @Test
    public void testGetAllTerminalCommands() {
        @Nullable final Collection<String> allTerminalCommands = this.commandRepository.getAllTerminalCommands();
        Assert.assertNotNull(allTerminalCommands);
    }

    @Test
    public void testGetAllArgumentCommands() {
        @Nullable final Collection<String> allArgumentCommands = this.commandRepository.getAllArgumentCommands();
        Assert.assertNotNull(allArgumentCommands);
    }

}