package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@Import({DataSourceConfiguration.class, SecurityConfiguration.class})
@ComponentScan(
        {
                "ru.renessans.jvschool.volkov.task.manager.service",
                "ru.renessans.jvschool.volkov.task.manager.repository"
        }
)
public class ApplicationConfiguration {
}