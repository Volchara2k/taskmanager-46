package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

public final class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @NotNull
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                ApplicationConfiguration.class,
                DataSourceConfiguration.class,
                SecurityConfiguration.class
        };
    }

    @NotNull
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                WebMvcConfiguration.class,
                WebRestConfiguration.class,
                WebSoapConfiguration.class
        };
    }

    @Override
    protected String @NotNull [] getServletMappings() {
        return new String[]{"/"};
    }

    @NotNull
    @Override
    protected Filter[] getServletFilters() {
        @NotNull final CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
        encodingFilter.setEncoding("UTF-8");
        encodingFilter.setForceEncoding(true);
        return new Filter[]{encodingFilter};
    }

}