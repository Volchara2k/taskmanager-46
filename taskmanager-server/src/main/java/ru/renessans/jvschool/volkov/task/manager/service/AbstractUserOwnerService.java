package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserOwnerService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.entity.AbstractUserOwner;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserOwnerRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Transactional
public abstract class AbstractUserOwnerService<E extends AbstractUserOwner> extends AbstractService<E> implements IUserOwnerService<E> {

    @NotNull
    private final IUserOwnerRepository<E> ownerUserRepository;

    @NotNull
    private final IUserService userService;

    protected AbstractUserOwnerService(
            @NotNull final IUserOwnerRepository<E> ownerUserRepository,
            @NotNull final IUserService userService
    ) {
        super(ownerUserRepository);
        this.ownerUserRepository = ownerUserRepository;
        this.userService = userService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E addUserOwner(
            @Nullable final E value
    ) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        @Nullable final User user = this.userService.getUserById(value.getUserId());
        value.setUser(user);
        return super.save(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateUserOwnerByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(newTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = this.getUserOwnerByIndex(userId, index);
        if (Objects.isNull(value)) throw new InvalidOwnerUserException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.save(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateUserOwnerById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        if (ValidRuleUtil.isNullOrEmpty(newTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = this.getUserOwnerById(userId, id);
        if (Objects.isNull(value)) throw new InvalidOwnerUserException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.save(value);
    }

    @SneakyThrows
    @Override
    public int deleteUserOwnerById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return this.ownerUserRepository.deleteByUserIdAndId(userId, id);
    }

    @SneakyThrows
    @Override
    public int deleteUserOwnerByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        @Nullable final E value = this.getUserOwnerByTitle(userId, title);
        if (Objects.isNull(value)) return 0;
        return this.ownerUserRepository.deleteByUserIdAndTitle(userId, title);
    }

    @SneakyThrows
    @Override
    public int deleteUserOwnerAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        return this.ownerUserRepository.deleteAllByUserId(userId);
    }

    @SneakyThrows
    @Override
    public int deleteUserOwnerByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        @NotNull final List<E> values = this.getUserOwnerAll(userId);
        if (index >= values.size()) return 0;
        @Nullable final E value = values.get(index);
        if (Objects.isNull(value)) return 0;
        return super.deleteRecordById(value.getId());
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getUserOwnerByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        @NotNull final List<E> values = this.getUserOwnerAll(userId);
        if (index >= values.size()) return null;
        return values.get(index);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getUserOwnerById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return this.ownerUserRepository.getUserOwnerById(userId, id);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getUserOwnerByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        return this.ownerUserRepository.getUserOwnerByTitle(userId, title);
    }

    @NotNull
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public List<E> getUserOwnerAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        return this.ownerUserRepository.getUserOwnerAll(userId);
    }

    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public long countUserOwner(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        return this.ownerUserRepository.countUserOwner(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> initialUserOwner(
            @Nullable final Collection<User> users
    ) {
        if (Objects.isNull(users)) throw new InvalidUserException();

        @NotNull final List<E> demoData = new ArrayList<>();
        users.forEach(user -> {
            final long countUserOwner = this.countUserOwner(user.getId());
            if (ValidRuleUtil.isNullOrEmpty(countUserOwner)) {
                @NotNull final E data = addUserOwner(
                        user.getId(), DemoDataConst.DEMO_TITLE_1ST,
                        DemoDataConst.DEMO_DESCRIPTION_2ND
                );
                demoData.add(data);
            }
        });

        return demoData;
    }

    @PostConstruct
    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> initialUserOwner() {
        @NotNull final List<E> demoData = new ArrayList<>();

        DemoDataConst.USERS_LOGINS.forEach(login -> {
            @Nullable final User user = this.userService.getUserByLogin(login);
            if (Objects.isNull(user)) return;
            final long countUserOwner = this.countUserOwner(user.getId());
            if (ValidRuleUtil.isNullOrEmpty(countUserOwner)) {
                @NotNull final E data = addUserOwner(
                        user.getId(), DemoDataConst.DEMO_TITLE_1ST,
                        DemoDataConst.DEMO_DESCRIPTION_2ND
                );
                demoData.add(data);
            }
        });

        return demoData;
    }

}