package ru.renessans.jvschool.volkov.task.manager.exception.invalid.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidUserIdException extends AbstractException {

    @NotNull
    private static final String EMPTY_USER_ID = "Ошибка! Параметр \"идентификатор пользователя\" отсутствует!\n";

    public InvalidUserIdException() {
        super(EMPTY_USER_ID);
    }

}