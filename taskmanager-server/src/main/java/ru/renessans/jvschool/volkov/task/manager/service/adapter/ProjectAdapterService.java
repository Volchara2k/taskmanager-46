package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.model.entity.TimeFrame;

import java.util.Date;
import java.util.Objects;

@Service
public final class ProjectAdapterService implements IProjectAdapterService {

    @Nullable
    @Override
    public ProjectDTO toDTO(@Nullable final Project convertible) {
        if (Objects.isNull(convertible)) return null;
        return ProjectDTO.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(
                        TimeFrameDTO.builder()
                                .creationDate(convertible.getTimeFrame().getCreationDate())
                                .build()
                )
                .status(convertible.getStatus())
                .build();
    }

    @Nullable
    @Override
    public Project toModel(@Nullable final ProjectDTO convertible) {
        if (Objects.isNull(convertible)) return null;

        if (Objects.isNull(convertible.getTimeFrame()) || Objects.isNull(convertible.getTimeFrame().getCreationDate()))
            convertible.setTimeFrame(
                    TimeFrameDTO.builder()
                            .creationDate(new Date())
                            .build()
            );

        return Project.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(
                        TimeFrame.builder()
                                .creationDate(Objects.requireNonNull(convertible.getTimeFrame().getCreationDate()))
                                .build()
                )
                .status(!Objects.isNull(convertible.getStatus()) ? convertible.getStatus() : UserOwnerStatus.NOT_STARTED)
                .build();
    }

}