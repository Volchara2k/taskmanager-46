package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.*;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Service
@Transactional
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(
            @NotNull final IUserRepository userRepository
    ) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        @NotNull final User user = new User(login, passwordHash);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, firstName);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRoleType userRoleType
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (Objects.isNull(userRoleType)) throw new InvalidUserRoleException();
        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, userRoleType);
        return super.save(user);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public User getUserById(
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return super.getRecordById(id);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public User getUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        return this.userRepository.getUserByLogin(login);
    }

    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public boolean existsUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        return this.userRepository.existsByLogin(login);
    }

    @NotNull
    @Transactional(readOnly = true)
    @Override
    public UserRoleType getUserRole(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) return UserRoleType.UNKNOWN;
        @Nullable final User user = this.getUserById(userId);
        if (Objects.isNull(user)) return UserRoleType.UNKNOWN;
        return user.getRole();
    }

    @NotNull
    @SneakyThrows
    @Override
    public User updateUserPasswordById(
            @Nullable final String id,
            @Nullable final String newPassword
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(newPassword)) throw new InvalidPasswordException();

        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        @NotNull final String passwordHash = HashUtil.getSaltHashLine(newPassword);
        user.setPasswordHash(passwordHash);

        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User editUserProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setFirstName(firstName);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User editUserProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        if (ValidRuleUtil.isNullOrEmpty(lastName)) throw new InvalidLastNameException();

        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);

        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User lockUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException();
        user.setLockdown(true);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User unlockUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException();
        user.setLockdown(false);
        return super.save(user);
    }

    @SneakyThrows
    @Override
    public int deleteUserById(
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        return super.deleteRecordById(id);
    }

    @SneakyThrows
    @Override
    public int deleteUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        return this.userRepository.deleteByLogin(login);
    }

    @PostConstruct
    @NotNull
    @Override
    public Collection<User> initialDemoUsers() {
        @NotNull final Collection<User> nonRewritableResult = new ArrayList<>();

        DemoDataConst.USERS.forEach(user -> {
            @NotNull final String demoLogin = user.getLogin();
            final boolean existsDemoByLogin = this.existsUserByLogin(demoLogin);
            if (!existsDemoByLogin) {
                @NotNull final User addUser = super.save(user);
                nonRewritableResult.add(addUser);
            }
        });

        return nonRewritableResult;
    }

}