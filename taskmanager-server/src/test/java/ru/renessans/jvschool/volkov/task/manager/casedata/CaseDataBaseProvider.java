package ru.renessans.jvschool.volkov.task.manager.casedata;

@SuppressWarnings("unused")
public final class CaseDataBaseProvider {

    public Object[] invalidObjectCaseData() {
        return new Object[]{
                invalidLinesCaseData()[0]
        };
    }

    public Object[] invalidLinesCaseData() {
        return new Object[]{
                new Object[]{null},
                new Object[]{"    "},
                new Object[]{""}
        };
    }

    public Object[] validLinesCaseData() {
        return new Object[]{
                new Object[]{"fg6A0Induv"},
                new Object[]{"JyWYAYmqJf"},
                new Object[]{"nnerVw24PV"}
        };
    }

}