package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.util.*;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                DataMarshalizerUtilTest.class,
                DataSerializerUtilTest.class,
                EndpointUtilTest.class,
                FileUtilTest.class,
                HashUtilTest.class,
                SignatureUtilTest.class,
                ValidRuleUtilTest.class
        }
)
public abstract class AbstractUtilityTestRunner {
}