package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidFirstNameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserRoleException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class AuthenticationServiceTest {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IAuthenticationService authenticationService;

    @Before
    public void assertMainComponentsNotNullBefore() {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.authenticationService);
    }

    @Test
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeVerifyValidUserData() {
        @NotNull final InvalidLoginException loginThrown = assertThrows(
                InvalidLoginException.class,
                () -> this.authenticationService.verifyValidUserData("", "password")
        );
        Assert.assertNotNull(loginThrown);
        Assert.assertNotNull(loginThrown.getMessage());

        @NotNull final String tempLogin = UUID.randomUUID().toString();
        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
                InvalidPasswordException.class,
                () -> this.authenticationService.verifyValidUserData(tempLogin, "   ")
        );
        Assert.assertNotNull(passwordThrown);
        Assert.assertNotNull(passwordThrown.getMessage());
    }

    @Test
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeSignUp() {
        @NotNull final InvalidLoginException loginThrown = assertThrows(
                InvalidLoginException.class,
                () -> this.authenticationService.signUp(" ", "password")
        );
        Assert.assertNotNull(loginThrown);
        Assert.assertNotNull(loginThrown.getMessage());

        @NotNull final String tempLogin = UUID.randomUUID().toString();
        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
                InvalidPasswordException.class,
                () -> this.authenticationService.signUp(tempLogin, "        ")
        );
        Assert.assertNotNull(passwordThrown);
        Assert.assertNotNull(passwordThrown.getMessage());

        @NotNull final String tempPassword = UUID.randomUUID().toString();
        @NotNull final InvalidFirstNameException firstNameThrown = assertThrows(
                InvalidFirstNameException.class,
                () -> this.authenticationService.signUp(tempLogin, tempPassword, (String) null)
        );
        Assert.assertNotNull(firstNameThrown);
        Assert.assertNotNull(firstNameThrown.getMessage());

        @NotNull final InvalidUserRoleException roleThrown = assertThrows(
                InvalidUserRoleException.class,
                () -> this.authenticationService.signUp(tempLogin, tempPassword, (UserRoleType) null)
        );
        Assert.assertNotNull(roleThrown);
        Assert.assertNotNull(roleThrown.getMessage());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testVerifyValidPermission() {
        @Nullable final User user = this.userService.getUserByLogin(DemoDataConst.USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        @NotNull final PermissionValidState permissionValidState =
                this.authenticationService.verifyValidPermission(user.getId(), user.getRole());
        Assert.assertNotNull(permissionValidState);
        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testVerifyValidUserData() {
        @NotNull final UserDataValidState userDataValidState = this.authenticationService.verifyValidUserData(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(userDataValidState);
        Assert.assertEquals(UserDataValidState.SUCCESS, userDataValidState);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testSignUp() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);

        @NotNull final User addUser = this.authenticationService.signUp(
                login, password
        );
        Assert.assertNotNull(addUser);
        Assert.assertEquals(addUser.getId(), addUser.getId());
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
        Assert.assertEquals(addUser.getRole(), addUser.getRole());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testSignUpWithUserRole() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserRoleType userRole = UserRoleType.USER;

        @NotNull final User addUser = this.authenticationService.signUp(login, password, userRole);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
        Assert.assertEquals(userRole, addUser.getRole());
    }

}