package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.UUID;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class UserServiceTest {

    @NotNull
    @Autowired
    private IUserService userService;
    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.userService);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetUserByLogin() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
        Assert.assertEquals(UserRoleType.USER, user.getRole());
    }


    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetUserRole() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final UserRoleType userRole = this.userService.getUserRole(user.getId());
        Assert.assertNotNull(userRole);
        Assert.assertEquals(user.getRole(), userRole);
    }


    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testAddUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);

        @NotNull final User addUser = this.userService.addUser(login, password);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testAddUserWithFirstName() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final String firstName = UUID.randomUUID().toString();
        Assert.assertNotNull(firstName);

        @NotNull final User addUser = this.userService.addUser(login, password, firstName);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
        Assert.assertEquals(firstName, addUser.getFirstName());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testAddUserWithRole() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserRoleType userRoleType = UserRoleType.USER;
        Assert.assertNotNull(userRoleType);

        @NotNull final User addUser = this.userService.addUser(login, password, userRoleType);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
        Assert.assertEquals(userRoleType, addUser.getRole());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUpdatePasswordById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addUser = this.userService.addUser(login, password);
        Assert.assertNotNull(addUser);

        @NotNull final String newPassword = UUID.randomUUID().toString();
        Assert.assertNotNull(newPassword);
        @Nullable final User updatePassword = this.userService.updateUserPasswordById(addUser.getId(), newPassword);
        Assert.assertNotNull(updatePassword);
        Assert.assertEquals(addUser.getId(), updatePassword.getId());
        Assert.assertEquals(login, updatePassword.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(newPassword);
        Assert.assertEquals(addUser.getRole(), updatePassword.getRole());
        Assert.assertEquals(hashPassword, updatePassword.getPasswordHash());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testEditProfileById() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String newFirstName = UUID.randomUUID().toString();
        Assert.assertNotNull(newFirstName);

        @Nullable final User editUser = this.userService.editUserProfileById(user.getId(), newFirstName);
        Assert.assertNotNull(editUser);
        Assert.assertEquals(user.getId(), editUser.getId());
        Assert.assertEquals("test", editUser.getLogin());
        Assert.assertEquals(user.getRole(), editUser.getRole());
        Assert.assertEquals(newFirstName, editUser.getFirstName());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testLockUserByLogin() {
        @Nullable final User lockUser = this.userService.lockUserByLogin("user");
        Assert.assertNotNull(lockUser);
        Assert.assertEquals("user", lockUser.getLogin());
        Assert.assertEquals(UserRoleType.USER, lockUser.getRole());
        Assert.assertTrue(lockUser.getLockdown());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUnlockUserByLogin() {
        @Nullable final User lockUser = this.userService.lockUserByLogin("user");
        Assert.assertNotNull(lockUser);

        @Nullable final User unlockUser = this.userService.unlockUserByLogin("user");
        Assert.assertNotNull(unlockUser);
        Assert.assertEquals("user", unlockUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine("user");
        Assert.assertEquals(hashPassword, unlockUser.getPasswordHash());
        Assert.assertEquals(UserRoleType.USER, unlockUser.getRole());
        Assert.assertFalse(unlockUser.getLockdown());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteUserByLogin() {
        final int deleteUser = this.userService.deleteUserByLogin("user");
        Assert.assertEquals(1, deleteUser);
    }

//    @Test(expected = InvalidLoginException.class)
//    @TestCaseName("Run testNegativeGetUserByLogin for getUserByLogin(\"{0}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataBaseProvider.class,
//            method = "invalidLinesCaseData"
//    )
//    public void testNegativeGetUserByLogin(
//            @Nullable final String login
//    ) {
//        Assert.assertNotNull(this.userService);
//        this.userService.getUserByLogin(login);
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeAddUser for addUser(\"{0}\", \"{1}\", \"{2}\", {3})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersAllFieldsCaseData"
//    )
//    public void testNegativeAddUser(
//            @Nullable final String login,
//            @Nullable final String password,
//            @Nullable final String firstName,
//            @Nullable final UserRole userRole
//    ) {
//        Assert.assertNotNull(this.userService);
//
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> this.userService.addUser(login, password)
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> this.userService.addUser(tempLogin, password)
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//
//        @NotNull final String tempPassword = DemoDataConst.USER_DEFAULT_PASSWORD;
//        @NotNull final InvalidFirstNameException firstNameThrown = assertThrows(
//                InvalidFirstNameException.class,
//                () -> this.userService.addUser(tempLogin, tempPassword, firstName)
//        );
//        Assert.assertNotNull(firstNameThrown);
//        Assert.assertNotNull(firstNameThrown.getMessage());
//
//        @NotNull final InvalidUserRoleException roleThrown = assertThrows(
//                InvalidUserRoleException.class,
//                () -> this.userService.addUser(tempLogin, tempPassword, userRole)
//        );
//        Assert.assertNotNull(roleThrown);
//        Assert.assertNotNull(roleThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeUpdatePasswordById for updatePasswordById(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeUpdatePasswordById(
//            @Nullable final String id,
//            @Nullable final String newPassword
//    ) {
//        Assert.assertNotNull(this.userService);
//
//        @NotNull final InvalidUserIdException idThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.userService.updatePasswordById(id, newPassword)
//        );
//        Assert.assertNotNull(idThrown);
//        Assert.assertNotNull(idThrown.getMessage());
//
//        @NotNull final String tempId = UUID.randomUUID().toString();
//        Assert.assertNotNull(tempId);
//        @NotNull final InvalidPasswordException newPasswordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> this.userService.updatePasswordById(tempId, newPassword)
//        );
//        Assert.assertNotNull(newPasswordThrown);
//        Assert.assertNotNull(newPasswordThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeEditProfileById for editProfileById(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeEditProfileById(
//            @Nullable final String id,
//            @Nullable final String firstName
//    ) {
//        Assert.assertNotNull(this.userService);
//
//        @NotNull final InvalidUserIdException idThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.userService.editProfileById(id, firstName)
//        );
//        Assert.assertNotNull(idThrown);
//        Assert.assertNotNull(idThrown.getMessage());
//
//        @NotNull final String tempFileName = DemoDataConst.USER_DEFAULT_FIRSTNAME;
//        Assert.assertNotNull(tempFileName);
//        @NotNull final InvalidFirstNameException fileNameThrown = assertThrows(
//                InvalidFirstNameException.class,
//                () -> this.userService.editProfileById(tempFileName, firstName)
//        );
//        Assert.assertNotNull(fileNameThrown);
//        Assert.assertNotNull(fileNameThrown.getMessage());
//    }
//
//    @Test(expected = InvalidLoginException.class)
//    @TestCaseName("Run testNegativeLockUserByLogin for lockUserByLogin(\"{0}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataBaseProvider.class,
//            method = "invalidLinesCaseData"
//    )
//    public void testNegativeLockUserByLogin(
//            @Nullable final String login
//    ) {
//        Assert.assertNotNull(this.userService);
//        this.userService.lockUserByLogin(login);
//    }
//
//    @Test(expected = InvalidLoginException.class)
//    @TestCaseName("Run testNegativeUnlockUserByLogin for unlockUserByLogin(\"{0}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataBaseProvider.class,
//            method = "invalidLinesCaseData"
//    )
//    public void testNegativeUnlockUserByLogin(
//            @Nullable final String login
//    ) {
//        Assert.assertNotNull(this.userService);
//        this.userService.unlockUserByLogin(login);
//    }
//
//    @Test(expected = InvalidLoginException.class)
//    @TestCaseName("Run testNegativeDeleteUserByLogin for deleteUserByLogin(\"{0}\")")
//    @Category(NegativeImplementation.class)
//    @Parameters(
//            source = CaseDataBaseProvider.class,
//            method = "invalidLinesCaseData"
//    )
//    public void testNegativeDeleteUserByLogin(
//            @Nullable final String login
//    ) {
//        Assert.assertNotNull(this.userService);
//        this.userService.deleteUserByLogin(login);
//    }

}