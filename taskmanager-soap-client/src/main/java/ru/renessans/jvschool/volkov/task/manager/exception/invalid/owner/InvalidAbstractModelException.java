package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidAbstractModelException extends AbstractException {

    @NotNull
    private static final String EMPTY_OWNER =
            "Ошибка! Параметр \"сущность\" отсутствует!\n";

    public InvalidAbstractModelException() {
        super(EMPTY_OWNER);
    }

}