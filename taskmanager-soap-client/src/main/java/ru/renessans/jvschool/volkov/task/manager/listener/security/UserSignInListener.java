package ru.renessans.jvschool.volkov.task.manager.listener.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.AuthenticationSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class UserSignInListener extends AbstractSecurityListener {

    @NotNull
    private static final String CMD_SIGN_IN = "sign-in";

    @NotNull
    private static final String DESC_SIGN_IN = "войти в систему";

    @NotNull
    private static final String NOTIFY_SIGN_IN =
            "Происходит попытка инициализации авторизации пользователя. \n" +
                    "Для авторизации пользователя в системе введите логин и пароль: ";

    public UserSignInListener(
            @NotNull final AuthenticationSoapEndpoint sessionEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(sessionEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_SIGN_IN;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_SIGN_IN;
    }

    @Async
    @Override
    @EventListener(condition = "@userSignInListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_SIGN_IN);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final String password = ViewUtil.getLine();
        final boolean loginResponse = super.authenticationEndpoint.login(login, password);
        super.sessionService.saveCookieHeaders(super.authenticationEndpoint);
        ViewUtil.print(loginResponse);
    }

}