package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidPortException extends AbstractException {

    @NotNull
    private static final String EMPTY_SESSION =
            "Ошибка! Параметр \"порт соединения\" отсутствует!\n";

    public InvalidPortException() {
        super(EMPTY_SESSION);
    }

}